from django.shortcuts import render
from todos.models import TodoList
from todos.models import TodoItem


def Todo_List(request):
    Todo_List = TodoList.objects.all()
    context = {
        "Todo_List": Todo_List,
    }
    return render(request, "Todo_list/list.html", context)


def Todo_Item(request):
    Todo_Item = TodoItem.objects.all()
    context = {
        "Todo_Item": Todo_Item,
    }
    return render(request, "Todo_item/list.html", context)
