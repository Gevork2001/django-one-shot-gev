from django.urls import path
from django.contrib import admin
from todos.views import Todo_List
from todos.views import Todo_Item


urlpatterns = [
    path("", Todo_List, name="Todo_List"),
    path("todoitem", Todo_Item, name="Todo_Item"),
]
